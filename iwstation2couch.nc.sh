#!/bin/sh

## you need a file /etc/iwstationdump.conf with the following values
#AUTH=d3JpdGU6U2NocmVpYkxpbmtl
#DATABASE=mydatabase
#PORT=5984
#HOST=mycouchdb.host.name
MYDEBUG=false
### to get the auth string 
#curl -sv --user name:password localhost
#
#In the header output you will see a line like this:
#> Authorization: Basic bmFtZTpwYXNzd29yZA==
#####################
## ##### e.g. #### AUTH=bmFtZTpwYXNzd29yZA==
####################
###
##
##### you might create the couch user before with 
#curl -s -X PUT http://localhost:5984/_users/org.couchdb.user:UserName \                                                                                                                                                      
# -H "Accept: application/json" \
# -H "Content-Type: application/json" \
# -d '{"_id": "org.couchdb.user:UserName", "name": "UserName", "password": "MyPassword", "roles": [], "type": "user"}' 
source /etc/iwstationdump.conf

POST_PATH="/${DATABASE}/" 
#BODY='{}'
#BODY_LEN=$( echo -n "${BODY}" | wc -c )

#echo -ne "POST ${POST_PATH} HTTP/1.0\r\nHost: ${HOST}\r\nAuthorization: Basic ${AUTH}\r\nContent-Type: application/json\r\nContent-Length: ${BODY_LEN}\r\n\r\n${BODY}" |   nc ${HOST} ${PORT}

_lookup_address() { nslookup "$1" 2>/dev/null | sed '/Server:/,/Name:/d'|grep -e ^Name -e ^$ -v|sed 's/Address\(\| \| .\):\( \|\)//g' | grep ^[0-9] ; }

_numeric_id_by_ip() { ip a|grep inet|grep -v -e fe80 -e 127 -e "inet6 fd" -e "inet6 ::1"|sed 's/192.168//g;s/^.\+inet\(6\|\) \(.\+\)\/.\+/\2/g'|tr -dc '[0-9a-f]' ; };

_send_couch() { 
		REQUEST=0
		while read BODY;do
		DOCID=$(date -u +%s)_$(tail -c3 /proc/uptime)-$(_numeric_id_by_ip |md5sum|cut -d" " -f1|head -c 18)_${REQUEST}
		#POST_PATH="/${DATABASE}/"'"'${DOCID}'"' ;	#POST_PATH="/${DATABASE}/${DOCID}" ;
		###SETTING DOCID in curl fails , so we insert it into our json
		FULLBODY=$(echo $BODY|sed 's/^{/{"_id":"'${DOCID}'",/g')
		############################# ^↑ line has to have a bracket in first place
		BODY=${FULLBODY}
		BODY_LEN=$( echo -n "${BODY}" | wc -c )
		
		$MYDEBUG && echo "WRITING:"${FULLBODY}" TO "${POST_PATH}
		REQUEST_DONE=false
		
		$MYDEBUG && echo "DION"lookup_address ${HOST}
		$MYDEBUG && _lookup_address ${HOST}
		## the following helps if people us DNS load balancing

		for hostip in $(_lookup_address ${HOST}) ;do  
			$MYDEBUG && echo "TRYING "$hostip
			echo REQUEST_DONE|grep -q -v false && ping -c 2 -i 1 $hostip -q &>/dev/null &&  echo -ne "POST ${POST_PATH} HTTP/1.0\r\nHost: ${HOST}\r\nAuthorization: Basic ${AUTH}\r\nContent-Type: application/json\r\nContent-Length: ${BODY_LEN}\r\n\r\n${FULLBODY}" |   nc ${hostip} ${PORT} |grep -e '"ok"' -e ^HTTP  && REQUEST_DONE=true ;
			### ↑ we didnt succeed already ↑  #####  ↑ so we ping ↑          #####    #### ## ^^ and finally issue our fake curl POST to couchdb ##
			
			done &  
			###inner for loop might spawn ( one line is one station )
			start=$(cat /proc/uptime );while true;do grep -q "^${start}$" /proc/uptime ||break ;done 
			#delay at least 1/100 sec so docid is not duplicated 
		let REQUEST+=1
				 done ; } ;
             


_iwstationdump() { iw dev|grep Interface|sed 's/.\+Interface //g'|while read wifinter;do iw dev $wifinter station dump;done |sed 's/\t/ /g' |sed 's/^Station.\+\(..:..:..:..:..:..\) .\+)/\t"station":"\1/g'|grep -e tx -e rx -e through -e signal -e ms -e itrate -e tation -e authorized  |sed 's/^ \+/,"/g;s/ /_/g;s/:_/":"/g;s/$/"/g;s/_\+-/-/g;s/\(MBit\/s\|Mbps\)//g;s/_dBm/dBm/g;s/_ms//g' |tr -d '\n'|sed 's/\t/\n\n/g'|grep -v ^$|sed 's/^/\{/g;s/$/\}\t/g' |tr -d '\n'|sed 's/\t/\n/g';echo ; } ;

_iwstationdump |grep -v ^$| _send_couch
